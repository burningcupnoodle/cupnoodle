You can download custom characters at:

http://solgryn.org/Stuff/IWBTB/Characters/

To create custom characters:

- Create a folder with the characters name (fx. "Mario")
- Make images with the filenames
	- stop
	- walk
	- jump
	- fall
	- shoot
	- bullet
	- openchest
	- itemget
	
	Each filename should have a number at the end, indicating the number of the frame (for example: stop1, stop2, stop3)

	(You can use the templates to place the character, the middle of the image is where the ground is)

- Make sounds with the filenames (optional)
	- jump.wav
	- doublejump.wav
	- shoot.wav
	- theme.ogg
	- walk.wav

- ingame you can select your character and edit the speed of the animations and how many frames they are